<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use \Curl\Curl;
use App\Jobs\QueueJob;
use DateTime;

class QueueTest3 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:queueTest3';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $date = new DateTime($this->ask('請輸入要查詢的日期("YYYY-MM-DD")，預設為今日'));

        $date = $this->verifyDate($date);

        $start_time = new DateTime($this->ask('請輸入起始時間("HH:mm 24小時制")，預設為00:00'));

        $end_time = new DateTime($this->ask('請輸入結束時間("HH:mm 24小時制"，預設為現在時間)'));

        $start_time = $this->verifyTime($start_time,$end_time);

        $end_time = $end_time->format('H:i');

        $start_date = new DateTime("$date $start_time");

        $end_date = new DateTime("$date $end_time");

        $start_timestamp = $start_date->getTimestamp();

        $end_timestamp = $end_date->getTimestamp();

        if ($start_timestamp > $end_timestamp) {
            $this->error('起始時間大於結束時間!');
            die();
        }

        $minute = ($end_timestamp - $start_timestamp)/60;
        $time = $start_time;

        for ($i = 1;$i <= $minute;$i++) {
            $url = "http://train.rd6/?start={$date}T{$time}:00&end={$date}T{$time}:59&from=0";
            echo $url. "\n";
            QueueJob::dispatch($url);
            $time = date("H:i", strtotime("{$time} +1 minutes"));
        }
    }

    protected function verifyDate($date)  
    {
        $year = $date->format('Y');
        $month = $date->format('m');
        $day = $date->format('d');

        if (!preg_match('/^2[0]\d{2}$/',$year)) {  //驗證年份
            $this->error('請輸入正確年份!');
            die();
        }

        if (!preg_match('/^0\d|1[0-2]$/',$month)) {   //驗證月份  
            $this->error('請輸入正確月份!');
            die();
        }

        switch ($month) {      //驗證日期
            case "01": case "03": case "05": case "07": case "08": case "10": case "12":
                $days = 31;
                if(!preg_match('/^0[1-9]|[1-2]\d|3[0-1]$/',$day)){     
                    $this->error('請輸入正確日期!');
                    die();
                }
                break;
            case "04": case "06": case "09": case "11":
                $days = 30;
                if(!preg_match('/^0[1-9]|[1-2]\d|30$/',$day)){     
                    $this->error('請輸入正確日期!');
                    die();
                }
                break;
            case "02":
                $days = 28;
                if(!preg_match('/^0[1-9]|[1-2]\d$/',$day)){     
                    $this->error('請輸入正確日期!');
                    die();
                }
                break;
            default:
                $days = -1;
        }

        return $date->format('Y-m-d');
    }

    protected function verifyTime($start_time,$end_time)  //驗證時間
    {   
        $now = new DateTime('now');

        $timestamp = $start_time->getTimestamp();
        $now_timestamp = $now->getTimestamp();

        if ($timestamp == $now_timestamp){
            $start_time->setTime(00, 00, 00);
        }

        $start_time = $start_time->format('H:i');
        $end_time = $end_time->format('H:i');

        if (!preg_match('/^([0-1]\d|2[0-3]):[0-5]\d$/',$start_time)) {     
            $this->error('請輸入正確時間格式!');
            die();
        }

        if (!preg_match('/^([0-1]\d|2[0-3]):[0-5]\d$/',$end_time)) {     
            $this->error('請輸入正確時間格式!');
            die();
        }

        return $start_time;
    }

}
