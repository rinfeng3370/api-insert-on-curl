<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Jobs\QueueJob;
use DateTime;

class QueueTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:queueTest';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $date;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
 
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = new DateTime($this->ask('請輸入要查詢的日期("YYYY-MM-DD")'));

        $date = $this->verifyDate($date);

        $time = new DateTime($this->ask('請輸入查詢時間("HH:mm 24小時制")'));
        
        $time = $this->verifyTime($time);

        $repair = false;

        if ($this->confirm('是否要進行補單')) {
            $repair = true;
        }
        
        for ($num = 0; $num < 10; $num ++) {
            $page = $num * 10000;
            $url="http://train.rd6/?start={$date}T{$time}:00&end={$date}T{$time}:59&from={$page}";
            echo $url . "\n";
            QueueJob::dispatch($url,$repair);
        }
    }

    protected function verifyDate($date)  
    {

        $year = $date->format('Y');
        $month = $date->format('m');
        $day = $date->format('d');

        if (!preg_match('/^2[0]\d{2}$/',$year)) {  //驗證年份
            $this->error('請輸入正確年份!');
            die();
        }

        if (!preg_match('/^0\d|1[0-2]$/',$month)) {   //驗證月份  
            $this->error('請輸入正確月份!');
            die();
        }

        switch ($month) {      //驗證日期
            case "01": case "03": case "05": case "07": case "08": case "10": case "12":
                if(!preg_match('/^0[1-9]|[1-2]\d|3[0-1]$/',$day)){     
                    $this->error('請輸入正確日期!');
                    die();
                }
                break;
            case "04": case "06": case "09": case "11":
                if(!preg_match('/^0[1-9]|[1-2]\d|30$/',$day)){     
                    $this->error('請輸入正確日期!');
                    die();
                }
                break;
            case "02":
                if(!preg_match('/^0[1-9]|[1-2]\d$/',$day)){     
                    $this->error('請輸入正確日期!');
                    die();
                }
                break;
            default:
                $days = -1;
        }

        return $date->format('Y-m-d');
    }

    protected function verifyTime($time)  //驗證時間
    {
        $now = new DateTime('now');

        $timestamp = $time->getTimestamp();
        $now_timestamp = $now->getTimestamp();

        if ($timestamp == $now_timestamp){
            $time->modify('-1 min');
        }

        $time = $time->format('H:i');
        if (!preg_match('/^([0-1]\d|2[0-3]):[0-5]\d$/',$time)) {     
            $this->error('請輸入正確時間格式!');
            die();
        }

        return $time;
    }
}
