<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use \Curl\Curl;
use App\Jobs\QueueJob;
use DateTime;

class QueueTest2 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:queueTest2';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $dateTime = new DateTime('now');

        $dateTime->modify('-1 min');
        
        $date = $dateTime->format('Y-m-d');
        
        $time = $dateTime->format('H:i');
        
        $url = "http://train.rd6/?start={$date}T{$time}:00&end={$date}T{$time}:59&from=0";
        
        QueueJob::dispatch($url);
    }
}
