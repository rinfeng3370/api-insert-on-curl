<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use DB;
use \Curl\Curl;
use Log;
use App\Queue;

class QueueJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $url;

    protected $retry = 0;

    protected $repair;

    public function __construct($url,$repair = false)
    {
        //
        $this->url = $url;

        $this->repair = $repair;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
        $curl = new Curl();
        
        echo $this->url. "\n";

        $curl->get($this->url);
        
        echo 'Response:' . "\n";

        $data = json_decode($curl->response,true);

        $pass = true;
        
        $array_key = array_keys($data);   //回傳error或沒資料時不會進到處理資料的流程
        if ($array_key[0] == "error" || $data['hits']['total']['value'] == 0) {
            echo  '發生錯誤或無資料!' . "\n";
            $pass = false;
        }

        if ($pass) {
            foreach($data['hits']['hits'] as $i => $value){
                $data['hits']['hits'][$i]['_source'] = stripcslashes(json_encode($value['_source']));
                $data['hits']['hits'][$i]['sort'] = stripcslashes(json_encode($value['sort']));
            }
    
            $arrayLength = count($data['hits']['hits']);
            echo "資料量: $arrayLength \n";
            
            $chunks = array_chunk($data['hits']['hits'],1000);
            
            if($this->repair){
                foreach ($chunks as $items) {
                    foreach ($items as $item) {
                        echo "補單中";
                        Queue::updateOrCreate($item);
                    }
                }
            } else {
                foreach ($chunks as $item) {
                    echo "insert ing";
                    DB::table('queues')->insert($item);
                }
            }


        }
        
        $curl->close();
        
    }
}
